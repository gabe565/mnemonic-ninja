<p align="center">
    <a href="https://mnemonic.ninja">
        <img src="/frontend/src/assets/logo.png?raw=true" title="Mnemonic Ninja Logo" height="120">
    </a>
</p>

# Mnemonic Ninja

Website to convert between a number and its corresponding word to aid in memorization. Unlike some of the other sites that I had tried out, this one processes conversions by looking at the phonetic pronunciation of a word and not just converting by specific letter.

View the live site at <https://mnemonic.ninja/>
